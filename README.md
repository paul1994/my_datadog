# my_datadog

Example cookbook that installs the datadog agent and configures IIS and apache webserver
to be monitored by datadog.

## SCOPE

Basic configuration to get started with Datadog

## Requirements


## Dependencies

Depends on the datadog cookbook
https://github.com/DataDog/chef-datadog/blob/master/README.md


## Usage

The following Assumptions will be made:
- You are currently using chef and have a Chef environment configured
- You have a workstation with chef-workstation installed
- Basic experience with creating / editing cookbooks

To use this example cookbook you must have vagrant and virtualbox configured locally or the AWS CLI and the kitchen ec2 driver installed.

Also the following Assumptions will be made:
    You are using chef and have a Chef environment configured
    You have a workstation with chef-workstation installed
    Basic experience with creating / editing cookbooks

If all the prerequisites are met the only change you will need to do in the cookbook will be to configure the api and application key for datadog located in the datadog.rb file in the attributes directory.

Example below:

data dog account info
default['datadog']['api_key'] = 'b4fa0fb05exxxxxxxxxxxxxxxxxx'
Use an existing application key or create a new one for Chef deployment
default['datadog']['application_key'] = 'c0b22xxxxxxxxxxxxxxxxxxxxxxx'


Note** 
If you are using AWS you will need to use the kitchen.aws.yml. This would need to be renamed to kitchen.yml and the aws specific items would need to be configured inside the file.

Also if you are using virtualbox you would need to rename the kitchen.vagrant.yml to kitchen.yml