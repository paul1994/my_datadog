name 'my_datadog'
maintainer 'P Bradford'
maintainer_email 'paul1994@hotmail.com'
license 'all rights reserved'
description 'Installs/Configures datadog agent'
version '0.1.0'

depends 'datadog'

issues_url 'https://gitlab.com/paul1994/my_datadog/issues'
source_url 'https://gitlab.com/paul1994/my_datadog'
