
# # data dog account info 
default['datadog']['api_key'] = 'b4fa0fb05exxxxxxxxxxxxxxxxxx'
# # Use an existing application key or create a new one for Chef deployment
default['datadog']['application_key'] = 'c0b22xxxxxxxxxxxxxxxxxxxxxxx'
# Set the Datadog site
default['datadog']['site'] = 'datadoghq.com'

# # to enable log monitoring
default['datadog']['enable_logs_agent'] = true

# IIS configs
default['datadog']['iis']['instances'] = [{
                                  'host' => 'localhost',
                                  'tags' => %w(prod dd_recruiting IIS),
                                  'sites' => ['Default Web Site'],
                                  }]
# Attributes used to configure IIS log gathering
default['datadog']['iis']['logs'] = [{
  'type' => 'file',
  'path' => 'C:\\inetpub\\logs\\LogFiles\\W3SVC1\\u_ex*',
  'source' => 'iis',
  'service' => 'iis',
  }]

# # Windows service
default['datadog']['windows_service']['instances'] = [
    {
      "services": ['AmazonSSMAgent'],
      "host": '.',
    },
]

# # Apache attributes
default['datadog']['apache']['instances'] = [{
  'status_url' => 'http://localhost',
  'tags' => %w(prod dd_recruiting apache_test),
  }]
# Apache config to collect Apache log if not using custom template
default['datadog']['apache']['logs'] = [{
  'type' => 'file',
  'path' => '/var/log/apache2/access.log',
  'source' => 'apache',
  'service' => 'apache',
  }]
