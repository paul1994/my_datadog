#
# Cookbook:: my_datadog
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

include_recipe 'datadog::dd-agent'
include_recipe 'datadog::dd-handler'

case node['platform']
when 'windows'
  winfeat = %w(
    web-server
    Web-Mgmt-Tools
    Web-Mgmt-Console
    Web-Ftp-Server
    )

  winfeat.each do |feat|
    dsc_resource 'Install some features' do
      resource :WindowsFeature
      property :Name, feat
      property :ensure, 'present'
    end
  end
  # Temp fix to set permissions for IIS logs
  execute 'ical permissions' do
    command 'icacls C:\inetpub\logs\LogFiles\W3SVC1\ /T /C /grant Everyone:(F)'
    action :run
  end
  # Temp page for default IIS site
  template 'C:/inetpub/wwwroot/index.htm' do
    source 'test.htm.erb'
    action :create
  end
  
  # installs the iis integration
  # Attributes are used to collect the IIS Logs
  include_recipe 'datadog::iis'
  include_recipe 'datadog::windows_service'
when 'debian', 'ubuntu'
  package 'apache2' do
    action :install
  end
  service 'apache2' do
    action [:start, :enable]
  end
  # Temp fix to configure apache logs
  execute 'change apache log ownership' do
    command 'chmod -R 755 /var/log/apache2/'
    action :run
  end
  # instead of ddog apache recipe using a test conf file for apache
  template '/etc/datadog-agent/conf.d/apache.d/conf.yaml' do
    source 'pb_apache.yaml.erb'
    owner 'dd-agent'
    group 'root'
    mode '0600'
    action :create
    notifies :restart, 'service[datadog-agent]'
  end

when 'redhat', 'centos', 'fedora'
  # added for centos nodes if created
  package 'httpd' do
    action :install
  end
  service 'httpd' do
    action [:start, :enable]
  end
  execute 'change httpd log ownership' do
    command 'chmod -R 755 /var/log/httpd/'
    action :run
    notifies :restart, 'service[datadog-agent]'
  end

  directory '/etc/datadog-agent/conf.d/http.d' do
    owner 'dd-agent'
    group 'root'
    mode '0600'
    action :create
  end
  template '/etc/datadog-agent/conf.d/http.d/conf.yaml' do
    source 'pb_httpd.yaml.erb'
    owner 'dd-agent'
    group 'root'
    mode '0600'
    action :create
    notifies :restart, 'service[datadog-agent]'
  end
end

